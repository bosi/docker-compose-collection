#!/usr/bin/env bash
DIR="$(dirname ${0})/../"
cd ${DIR}

./bin/subscripts/check_env.sh
if [[ $? -ne 0 ]]; then exit 1; fi

COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
docker exec -it --user=www-data "${COMPOSE_PROJECT_NAME}_php-fpm_1" /bin/bash ${@}
cd - > /dev/null