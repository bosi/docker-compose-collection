#!/usr/bin/env bats

load helpers

run_script () {
    run bin/subscripts/docker-compose.sh ${@}
}

@test $(get_description "check if command works in general") {
    run_script --version
    [ $status -eq 0 ]
    [ $(echo $output | grep -i "version" | wc -l) -eq 1 ]
}

@test $(get_description "check if ports are loaded correctly") {
    sed -i "s|^\(LINK_PORTS=\s*\).*$|\1true|" .env
    run_script up -d mailhog
    run docker ps
    [ $(echo $output | grep -i '0.0.0.0:' | wc -l) -eq 1 ]
}

@test $(get_description "check if ports are not loaded when LINK_PORTS is set to false") {
    sed -i "s|^\(LINK_PORTS=\s*\).*$|\1false|" .env
    run_script up -d mailhog
    run docker ps
    [ $(echo $output | grep -i '0.0.0.0:' | wc -l) -eq 0 ]
}

@test $(get_description "check if override file is used when present") {
    printf "version: '3'\nservices:\n    mailhog:\n        ports:\n        - 54321:8025\n" > docker-compose-override.yml
    run_script up -d mailhog
    run docker ps
    [ $(echo $output | grep -i '54321' | wc -l) -eq 1 ]
}
